 

 

 

 

 

przeciwko wszystkim«. Wieków trzeba było,
żeby »treuga Dei« ograniczała mstę stopniowo;
a w ciągu tych pokoleń panujący zasiadał na
sądach tylko na żądanie strony. Taki, który
udawał się do swego księcia, żeby go wyręczył
w mście i ukarał krzywdziciela, narażał na hańbę
nie tylko siebie, lecz cały swój ród, bo usuwał
się od osobistego spełniania swych obowiązków
i uznawał się być słabym. Długo też wyręczały
się sądownictwem książęcym same tylko nie-
wiasty, gdy im zabrakło męskiego mściciela.
Działała »treuga Deix i robiła swoje, rozszerzana
coraz bardziej, lecz jakżeż powoli i pod jakimże
naciskiem kar kościelnych i nierzadkich klątw!
W Polsce już za pierwszych Piastów wprowa-
dzano za zabójstwo grzywny pieniężne, lecz
krwawe zwady rodów zdarzały się jeszcze w XIV
w. W Niemczech zaś dopiero na samym końcu
XV w. ogłoszono »Landfrieden«, w czym mie-
ścił się zakaz wykonywania msty i przymus
przekazywania krwawych sporów władzy pań-
stwowej. Minęło jednak jeszcze nieco czasu, za-
nim ustawa zdobyła sobie powszechne uznanie.

Wykonywanie msty, dziedziczne w rodach
z pokolenia w pokolenie, wytworzyło i rozga-
łęziło długi szereg nadużyć i zbrodni; naduży-
wano bowiem msty i podszywano się chytrze
pod nią, okrywając jej płaszczem liczne prze-
stępstwa — aż do rozbojów włącznie. Ze stanu
wojennego między rodami wytwarzały się walki

 

 

= aaea n

  
