input_array = ARGV 
input_file = input_array[0] || './in.txt'
output_file = input_array[1] || './out.txt'

lines = []

File.foreach(input_file) do |line|
  lines.push line
end

out = []

lines.size.times do |i|
	lines[i].gsub!("-\n", "")
end

puts lines.join
