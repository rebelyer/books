 

 

  

 

nizmów społecznych. Nie da się wytworzyć
zrzeszenia, które by było równocześnie organiz-
mem i mechanizmem, bo mieszanina tego z tam-
tym posiada własności trujące ; stanowi truciznę
i dla państwa i dla społeczeństwa. Gdyby za-
panowało gdzieś powszechne pomieszanie me-
tod organicznych a mechanicznych, gdyby udzie-
lało się stopniowo wszystkim dziedzinom życia
zbiorowego, wyniknęłyby z tego następstwa ab-
surdalne a straszne. Wspólna cywilizacja sta-
nowi bowiem więź zrzeszeń: naruszanie przeto
zwartości cywilizacyjnej jest robotą destrukty-
wną, jest druzgotaniem -więzi i narodowej i pań-
stwowej. Jest to gonitwa za zerem. Dodajmy,
że mechanizm nie wytworzy moralności, oświa-
ty, ni dobrobytu.

Jeżeli więc chcemy się utrzymać przy cywi-
lizacji łacińskiej, musimy trzymać się persona-
lizmu, zasady rozmaitości, aposterioryzmu i or-
ganizmu z zasadą supremacji sił duchowych.

Tak jest i tak pozostać winno we wszystkich
społeczeństwach, które »wychowywał Kościół«,
ten Kościół, którego dziełem jest cywilizacja
łacińska.

Do cech państwowości łacińskiej należy od-
rębne prawo państwowe, odrębność prawa pu-
blicznego w ogóle. Co innego prawo prywatne,
co innego publiczne. Ten dualizm prawny na-
leży do węgłów naszej cywilizacji, a zatem

 

 

 

  
