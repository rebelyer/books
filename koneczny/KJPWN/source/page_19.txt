 

 

 

III. ZASADY ŻYCIA PUBLICZNEGO
NARODÓW.

Metody życia zbiorowego kształcą się w ka-
tolicyzmie, doskonalą się w miarę, jak zbliżają
się do ideału »Civitatis Dei«, a gdy się od niego
odchylają, obniżają się i psują. W każdym
razie są zmienne. Jak wiadomo, Kościół nie
identyfikuje się z żadną specjalną formą rzą- -
dów, wymaga tylko od każdej, żeby zachowy-
wała moralność według etyki katolickiej. Ale
pośród wielości najrozmaitszych sposobów rzą-

  
  
 
 
 
 
 
 
 
 
 

 

 

  

16

  
