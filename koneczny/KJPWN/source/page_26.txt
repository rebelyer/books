 

 

społeczeństwem. Z silnego społeczeństwa wy-
łania się silne państwo samo przez się. W na-
szej cywilizacji niema obawy o to, iżby siła
społeczna mogła nie wydać siły politycznej;
lecz na nic wszelkie zachody o potęgę państwa,
gdzie społeczeństwo osłabione. Albowiem pań-
stwo w cywilizacji łacińskiej jest organizmem,
wytwarza się więc w sposób naturalny ze spo-
łecznego podłoża. Wszelkie próby, żeby pań-
stwo wyemancypować niejako od społeczeństwa,
dezorganizują tylko społeczeństwo, mącą i ob-
niżają stan cywilizacyjny.

Państwo, wychylające się ze wspólnoty cy-
wilizacyjnej ze społeczeństwem, należącym do
cywilizacji łacińskiej, musi się stawać coraz
bardziej mechanizmem i przystąpi do walki
z personalizmem. A zatem nastałby roziam po-
między państwem a społeczeństwem, co stano-
wiłoby klęskę najcięższą dla obu.

Przyczyna istotna (owa przyczyna przyczyn)
takiego związku tych spraw w naszej cywiliza-
cji jest bardzo prosta. Nie można robić jednej
i tej samej rzeczy równocześnie dwiema meto-
dami: prawo to sięga od robót najgrubszych
i najłatwiejszych aż do wyżyn twórczości pań-
stwowej. W ten błąd popadłoby się jednak,
gdyby się chciało osadzić państwo mechaniz-
mowe na podłożu organicznym, gdzie społe-
czeństwo (naród) wytworzyło się i rozkwitać
ma nadal z jednoczenia się rozmaitych orga-

 

25

 
